package gousers

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type UserRepoGorm struct {
	db *gorm.DB
}

func (u *UserRepoGorm) Store(createUser CreateUser) (User, error) {
	gu := &gormUser{ID: uuid.New().String(), Email: createUser.Email, Name: createUser.Name, UserRole: createUser.UserRole, Password: createUser.Password, Avatar: "", Bio: ""}
	err := u.db.Create(gu).Error
	if err != nil {
		return User{}, err
	}

	res := User{ID: gu.ID, Email: gu.Email, Name: gu.Name, UserRole: gu.UserRole, CreatedAt: gu.CreatedAt, UpdatedAt: gu.UpdatedAt, Avatar: "", Bio: ""}
	return res, nil
}

func (u *UserRepoGorm) FindUser(id string) (User, error) {
	var gu gormUser
	err := u.db.Where(&gormUser{ID: id}).First(&gu).Error
	if err != nil {
		return User{}, err
	}

	return User{ID: gu.ID, Email: gu.Email, Name: gu.Name, UserRole: gu.UserRole, CreatedAt: gu.CreatedAt, UpdatedAt: gu.UpdatedAt, Avatar: gu.Avatar, Bio: gu.Bio}, nil
}

func (u *UserRepoGorm) GetForLogin(email string) (CreateUser, error) {
	var gu gormUser
	err := u.db.Where(&gormUser{Email: email}).First(&gu).Error
	if err != nil {
		return CreateUser{}, err
	}

	return CreateUser{User: User{ID: gu.ID, Email: gu.Email, Name: gu.Name, UserRole: gu.UserRole, CreatedAt: gu.CreatedAt, UpdatedAt: gu.UpdatedAt}, Password: gu.Password}, nil
}

func (u *UserRepoGorm) GetCount() (int64, error) {
	var count int64
	err := u.db.Model(&gormUser{}).Count(&count).Error
	return count, err
}

func (u *UserRepoGorm) ListUsers() ([]User, error) {
	var gUsers []gormUser

	err := u.db.Find(&gUsers).Error
	if err != nil {
		return nil, err
	}

	var results []User
	for _, gu := range gUsers {
		r := User{ID: gu.ID, Email: gu.Email, Name: gu.Name, UserRole: gu.UserRole, CreatedAt: gu.CreatedAt, UpdatedAt: gu.UpdatedAt}
		results = append(results, r)
	}
	return results, nil
}

func (u *UserRepoGorm) UpdateUser(user User) error {
	userUpdate := gormUser{ID: user.ID, Name: user.Name, Email: user.Email, Avatar: user.Avatar, Bio: user.Bio}
	return u.db.Updates(&userUpdate).Error
}

func NewUserRepoGorm(db *gorm.DB) UserRepo {
	userRepo := &UserRepoGorm{db: db}
	db.AutoMigrate(&gormUser{})
	return userRepo
}

type gormUser struct {
	ID        string `gorm:"type:varchar(48)"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
	Email     string     `gorm:"index:users_email_idx"`
	Name      string
	Avatar    string
	Bio       string `gorm:"type:text"`
	UserRole  int
	Password  string
}

func (gormUser) TableName() string {
	return "users"
}
