package gousers

type UserRepo interface {
	Store(createUser CreateUser) (User, error)
	FindUser(id string) (User, error)
	GetForLogin(email string) (CreateUser, error)
	GetCount() (int64, error)
	ListUsers() ([]User, error)
	UpdateUser(user User) error
}
