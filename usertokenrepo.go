package gousers

type UserTokenRepo interface {
	Store(userToken UserToken) (UserToken, error)
	FindUserToken(token string) (UserToken, error)
	FindById(tokenID string) (UserToken, error)
	ListUserTokens(userID string) ([]ApiUserToken, error)
	DeleteToken(token string) error
	DeleteTokenById(tokenID string) error
}
