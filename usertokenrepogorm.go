package gousers

import (
	"time"

	"github.com/google/uuid"

	"gorm.io/gorm"
)

type UserTokenRepoGorm struct {
	db *gorm.DB
}

func (u *UserTokenRepoGorm) Store(userToken UserToken) (UserToken, error) {
	gut := &gormUserToken{ID: uuid.New().String(), UserID: userToken.UserID, Token: userToken.Token, Fingerprint: userToken.Fingerprint, RememberMeToken: userToken.RememberMeToken}
	err := u.db.Create(gut).Error
	if err != nil {
		return UserToken{}, err
	}
	return UserToken{ID: gut.ID, UserID: gut.UserID, Token: gut.Token, Fingerprint: gut.Fingerprint, RememberMeToken: gut.RememberMeToken}, nil
}

func (u *UserTokenRepoGorm) FindUserToken(token string) (UserToken, error) {
	var gut gormUserToken
	err := u.db.Where(&gormUserToken{Token: token}).First(&gut).Error
	if err != nil {
		return UserToken{}, err
	}

	return UserToken{ID: gut.ID, Token: gut.Token, UserID: gut.UserID, Fingerprint: gut.Fingerprint, RememberMeToken: gut.RememberMeToken}, nil
}

func (u *UserTokenRepoGorm) FindById(tokenID string) (UserToken, error) {
	var gut gormUserToken
	err := u.db.Where(&gormUserToken{ID: tokenID}).First(&gut).Error
	if err != nil {
		return UserToken{}, err
	}

	return UserToken{ID: gut.ID, Token: gut.Token, UserID: gut.UserID, Fingerprint: gut.Fingerprint, RememberMeToken: gut.RememberMeToken}, nil
}

func (u *UserTokenRepoGorm) ListUserTokens(userID string) ([]ApiUserToken, error) {
	var tokens []gormApiUserToken
	err := u.db.Model(&gormUserToken{}).Where(&gormUserToken{UserID: userID}).Find(&tokens).Error
	if err != nil {
		return nil, err
	}

	var result []ApiUserToken
	for _, tok := range tokens {
		result = append(result, ApiUserToken{ID: tok.ID, Fingerprint: tok.Fingerprint})
	}
	return result, nil
}

func (u *UserTokenRepoGorm) DeleteToken(token string) error {
	return u.db.Where("token = ?", token).Delete(&gormUserToken{}).Error
}

func (u *UserTokenRepoGorm) DeleteTokenById(tokenID string) error {
	return u.db.Delete(&gormUserToken{ID: tokenID}).Error
}

func NewUserTokenRepoGorm(db *gorm.DB) UserTokenRepo {
	userRepo := &UserTokenRepoGorm{db: db}
	db.AutoMigrate(&gormUserToken{})
	return userRepo
}

type gormUserToken struct {
	ID              string `gorm:"type:varchar(48)"`
	CreatedAt       time.Time
	UpdatedAt       time.Time
	DeletedAt       *time.Time `sql:"index"`
	UserID          string     `gorm:"type:varchar(48);index:usertoken_userid_idx"`
	Token           string     `gorm:"type:varchar(512);index:usertoken_token_idx"`
	Fingerprint     string
	RememberMeToken string `gorm:"type:varchar(512);index:usertoken_rememberme_idx"`
}

type gormApiUserToken struct {
	ID          string
	Fingerprint string
}

func (gormUserToken) TableName() string {
	return "user_tokens"
}
