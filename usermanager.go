package gousers

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"log"

	"gitlab.com/kendellfab/web/messaging"
	"golang.org/x/crypto/bcrypt"
)

var ErrorAuthenticating = errors.New("error authenticating")
var ErrorUpdateNotAuthorized = errors.New("update not authorized")
var ErrorUserNotOwner = errors.New("user does not own token")

type UserManager struct {
	userRepo       UserRepo
	userTokenRepo  UserTokenRepo
	magicTokenRepo MagicTokenRepo
	msg            Messenger
	sender         messaging.Sender
	adminRole      int
	fromAddress    string
}

func NewUserManager(userRepo UserRepo, userTokenRepo UserTokenRepo, magicTokenRepo MagicTokenRepo, adminRole int, msg Messenger, sender messaging.Sender, fromAddress string) *UserManager {
	return &UserManager{userRepo: userRepo, userTokenRepo: userTokenRepo, magicTokenRepo: magicTokenRepo, msg: msg, sender: sender, adminRole: adminRole, fromAddress: fromAddress}
}

func (um *UserManager) CreateAdminUser(email, name, password string) (User, error) {

	pass, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)

	cu := CreateUser{User: User{Email: email, Name: name, UserRole: um.adminRole}, Password: string(pass)}
	return um.userRepo.Store(cu)
}

func (um *UserManager) TryLogin(email, password, fingerprint string) (User, UserToken, error) {

	u, uErr := um.userRepo.GetForLogin(email)
	if uErr != nil {
		return User{}, UserToken{}, uErr
	}

	passErr := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))
	if passErr != nil {
		return User{}, UserToken{}, errors.New("error logging in")
	}

	token, tkErr := GenerateRandomURLSafeString(128)
	if tkErr != nil {
		return User{}, UserToken{}, tkErr
	}

	rememberMe, rmErr := GenerateRandomURLSafeString(256)
	if rmErr != nil {
		return User{}, UserToken{}, rmErr
	}

	ut := UserToken{UserID: u.ID, Token: token, Fingerprint: fingerprint, RememberMeToken: rememberMe}

	utRes, err := um.userTokenRepo.Store(ut)
	if err != nil {
		return u.User, utRes, err
	}

	return u.User, utRes, nil
}

func (um *UserManager) InitiateLogin(email string) error {
	u, uErr := um.userRepo.GetForLogin(email)
	if uErr != nil {
		log.Println("error loading user:", uErr)
		return nil
	}

	token, tkErr := GenerateRandomURLSafeString(128)
	if tkErr != nil {
		log.Println("error generating magic token", tkErr)
		return nil
	}

	cryptToken, _ := bcrypt.GenerateFromPassword([]byte(token), bcrypt.DefaultCost)

	mtID, mtErr := um.magicTokenRepo.Store(u.ID, string(cryptToken))
	if mtErr != nil {
		log.Println("error storing magic token", mtErr)
		return nil
	}

	data := map[string]interface{}{"Token": token, "ID": mtID}
	var w bytes.Buffer
	msgErr := um.msg.RenderMessagePlain(&w,"magiclink", data)
	if msgErr != nil {
		log.Println("error generating message", msgErr)
		return nil
	}

	sendErr := um.sender.Send(messaging.Email{Name: u.Name, Address: u.Email}, messaging.Email{Name: "Login", Address: um.fromAddress}, messaging.Message{Plain: w.String(), Subject: "Login Requested"})
	if sendErr != nil {
		log.Println("error sending message", sendErr)
		return nil
	}

	return nil
}

func (um *UserManager) RedeemLogin(token, mtID, fingerprint string) (User, UserToken, error) {

	magicToken, mtErr := um.magicTokenRepo.Find(mtID)
	if mtErr != nil {
		log.Println("error loading magic token", mtErr)
		return User{}, UserToken{}, ErrorAuthenticating
	}

	tokErr := bcrypt.CompareHashAndPassword([]byte(magicToken.Token), []byte(token))
	if tokErr != nil {
		log.Println("error validating incoming token", tokErr)
		return User{}, UserToken{}, ErrorAuthenticating
	}

	usrTok, tkErr := GenerateRandomURLSafeString(128)
	if tkErr != nil {
		log.Println("error generating new user token", tkErr)
		return User{}, UserToken{}, ErrorAuthenticating
	}

	rememberMe, rmErr := GenerateRandomURLSafeString(256)
	if rmErr != nil {
		log.Println("error generating remember me token", rmErr)
		return User{}, UserToken{}, ErrorAuthenticating
	}

	ut := UserToken{UserID: magicToken.UserID, Token: usrTok, RememberMeToken: rememberMe, Fingerprint: fingerprint}
	utRes, err := um.userTokenRepo.Store(ut)
	if err != nil {
		log.Println("error storing new user token", err)
		return User{}, UserToken{}, ErrorAuthenticating
	}

	delErr := um.magicTokenRepo.Delete(mtID)
	if delErr != nil {
		log.Println("error deleting magic token", delErr)
	}

	return User{}, utRes, nil
}

func (um *UserManager) LoadUserByToken(token string) (User, error) {
	ut, err := um.userTokenRepo.FindUserToken(token)
	if err != nil {
		return User{}, err
	}

	return um.userRepo.FindUser(ut.UserID)
}

func (um *UserManager) UsersCount() (int64, error) {
	return um.userRepo.GetCount()
}

func (um *UserManager) UpdateUser(currentUser User, r io.Reader) error {
	var inputUser User
	err := json.NewDecoder(r).Decode(&inputUser)
	if err != nil {
		return err
	}

	if currentUser.UserRole != um.adminRole || currentUser.ID != inputUser.ID {
		return ErrorUpdateNotAuthorized
	}

	return um.userRepo.UpdateUser(inputUser)
}

func (um *UserManager) FindById(id string) (User, error) {
	return um.userRepo.FindUser(id)
}

func (um *UserManager) ListUserTokens(userID string) ([]ApiUserToken, error) {
	return um.userTokenRepo.ListUserTokens(userID)
}

func (um *UserManager) DeleteToken(user User, token string) error {
	tok, err := um.userTokenRepo.FindUserToken(token)
	if err != nil {
		return err
	}

	if tok.UserID != user.ID {
		return ErrorUserNotOwner
	}

	return um.userTokenRepo.DeleteToken(token)
}

func (um *UserManager) DeleteTokenById(user User, tokenID string) error {
	tok, err := um.userTokenRepo.FindById(tokenID)
	if err != nil {
		return err
	}
	if tok.UserID != user.ID {
		return ErrorUserNotOwner
	}
	return um.userTokenRepo.DeleteTokenById(tokenID)
}
