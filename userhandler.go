package gousers

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

const (
	defaultSessionKey       = "gousers-session"
	defaultAuthSuccessRoute = "/admin/dashboard"
	defaultLoginRoute       = "/login"
	defaultSetupRoute       = "/setup"

	defaultLoginTplKey = "front.login"
	defaultSetupTplKey = "front.setup"

	defaultNameFormKey     = "name"
	defaultEmailFormKey    = "email"
	defaultPasswordFormKey = "password"

	defaultTokenQuery = "token"
	defaultIdQuery    = "id"

	defaultSessionTokenKey = "token"

	defaultRememberMeCookieKey = "remember-me"
)

type ParamExtractor func(r *http.Request, name string) string
type UserHandlerConfig func(*UserHandler) error

type UserHandler struct {
	Renderer
	userManager         *UserManager
	paramExtractor      ParamExtractor
	sessionKey          string
	authSuccessRoute    string
	loginRoute          string
	setupRoute          string
	loginTplKey         string
	setupTplKey         string
	nameFormKey         string
	emailFormKey        string
	passwordFormKey     string
	tokenQuery          string
	idQuery             string
	sessionTokenKey     string
	rememberMeCookieKey string
}

func NewUserHandler(renderer Renderer, userManager *UserManager, paramExtractor ParamExtractor, opts ...UserHandlerConfig) *UserHandler {
	uh := &UserHandler{Renderer: renderer, userManager: userManager, paramExtractor: paramExtractor, sessionKey: defaultSessionKey, authSuccessRoute: defaultAuthSuccessRoute, loginRoute: defaultLoginRoute,
		setupRoute: defaultSetupRoute, loginTplKey: defaultLoginTplKey, setupTplKey: defaultSetupTplKey, nameFormKey: defaultNameFormKey,
		emailFormKey: defaultEmailFormKey, passwordFormKey: defaultPasswordFormKey, tokenQuery: defaultTokenQuery, idQuery: defaultIdQuery,
		sessionTokenKey: defaultSessionTokenKey, rememberMeCookieKey: defaultRememberMeCookieKey}

	for _, opt := range opts {
		opt(uh)
	}

	return uh
}

func (uh *UserHandler) ShowSetup(w http.ResponseWriter, r *http.Request) {
	count, _ := uh.userManager.UsersCount()
	if count > 0 {
		uh.SetErrorFlash(w, r, "user already created")
		uh.Redirect(w, r, uh.loginRoute)
		return
	}

	uh.RenderPage(w, r, uh.setupTplKey, nil)
}

func (uh *UserHandler) DoSetup(w http.ResponseWriter, r *http.Request) {
	count, _ := uh.userManager.UsersCount()
	if count > 0 {
		uh.SetErrorFlash(w, r, "user already created")
		uh.Redirect(w, r, uh.loginRoute)
		return
	}

	name := r.FormValue(uh.nameFormKey)
	email := r.FormValue(uh.emailFormKey)
	password := r.FormValue(uh.passwordFormKey)

	usr, err := uh.userManager.CreateAdminUser(email, name, password)
	if err != nil {
		uh.SetErrorFlash(w, r, err.Error())
		uh.Redirect(w, r, uh.setupRoute)
		return
	}

	uh.SetSuccessFlash(w, r, fmt.Sprintf("%s created", usr.Name))
	uh.Redirect(w, r, uh.loginRoute)
}

func (uh *UserHandler) ShowLogin(w http.ResponseWriter, r *http.Request) {
	uh.RenderPage(w, r, uh.loginTplKey, nil)
}

func (uh *UserHandler) DoLogin(w http.ResponseWriter, r *http.Request) {
	email := r.FormValue(uh.emailFormKey)

	uh.userManager.InitiateLogin(email)
	uh.SetSuccessFlash(w, r, "Check your email")

	uh.Redirect(w, r, uh.loginRoute)
}

func (uh *UserHandler) DoRedeemLogin(w http.ResponseWriter, r *http.Request) {
	token := r.URL.Query().Get(uh.tokenQuery)
	id := r.URL.Query().Get(uh.idQuery)

	_, usrTok, err := uh.userManager.RedeemLogin(token, id, r.UserAgent())
	if err != nil {
		uh.SetErrorFlash(w, r, err.Error())
		uh.Redirect(w, r, uh.loginRoute)
		return
	}

	sess, sessErr := uh.Get(r, uh.sessionKey)
	if sessErr != nil {
		uh.SetErrorFlash(w, r, sessErr.Error())
		uh.Redirect(w, r, uh.loginRoute)
		return
	}

	sess.Values[uh.sessionTokenKey] = usrTok.Token

	svErr := sess.Save(r, w)
	if svErr != nil {
		log.Println("Error saving session", svErr)
	}

	ck := &http.Cookie{Name: uh.rememberMeCookieKey, Value: usrTok.RememberMeToken, Expires: time.Now().AddDate(1, 0, 0)}
	http.SetCookie(w, ck)
	uh.Redirect(w, r, uh.authSuccessRoute)
}

func (uh *UserHandler) DoLogout(w http.ResponseWriter, r *http.Request) {
	token, ok := TokenFromContext(r)
	if !ok {
		uh.SetErrorFlash(w, r, "User not found")
		uh.Redirect(w, r, uh.loginRoute)
		return
	}

	usr, _ := FromContext(r)

	err := uh.userManager.DeleteToken(usr, token)
	if err != nil {
		log.Println("error removing token", err)
	}
	sess, sessErr := uh.Get(r, uh.sessionKey)
	if sessErr != nil {
		uh.SetErrorFlash(w, r, "error accessing session")
		uh.Redirect(w, r, uh.loginRoute)
		return
	}
	sess.Options.MaxAge = -1
	sess.Save(r, w)

	ck, ckErr := r.Cookie(uh.rememberMeCookieKey)
	if ckErr == nil {
		ck.MaxAge = -1
		http.SetCookie(w, ck)
	}

	uh.Redirect(w, r, uh.loginRoute)
}

func (uh *UserHandler) DoUpdateUser(w http.ResponseWriter, r *http.Request) {
	usr, ok := FromContext(r)
	if !ok {
		http.Error(w, "Authorized user required", http.StatusUnauthorized)
		return
	}

	updateErr := uh.userManager.UpdateUser(usr, r.Body)
	if updateErr != nil {
		if updateErr == ErrorUpdateNotAuthorized {
			http.Error(w, "Not Authorized", http.StatusUnauthorized)
		} else {
			http.Error(w, updateErr.Error(), http.StatusInternalServerError)
		}

		return
	}

	fmt.Fprint(w, "user updated")
}

func (uh *UserHandler) GetUserTokens(w http.ResponseWriter, r *http.Request) {
	usr, ok := FromContext(r)
	if !ok {
		http.Error(w, "user must be authenticated", http.StatusUnauthorized)
		return
	}

	tokens, err := uh.userManager.ListUserTokens(usr.ID)
	if err != nil {
		log.Println("error loading user tokens", err)
		http.Error(w, "could not find user tokens", http.StatusNotFound)
		return
	}

	uh.RenderJson(w, tokens)
}

func (uh *UserHandler) DeleteUserToken(w http.ResponseWriter, r *http.Request) {
	tokenID := uh.paramExtractor(r, "tid")
	if tokenID == "" {
		http.Error(w, "token id not present", http.StatusExpectationFailed)
		return
	}

	usr, ok := FromContext(r)
	if !ok {
		http.Error(w, "unauthorized", http.StatusUnauthorized)
		return
	}

	err := uh.userManager.DeleteTokenById(usr, tokenID)
	if err == ErrorUserNotOwner {
		http.Error(w, "unauthorized", http.StatusUnauthorized)
		return
	} else if err != nil {
		log.Println("error deleting token by id", err)
		http.Error(w, "unknown error", http.StatusInternalServerError)
		return
	}

	fmt.Fprint(w, "user token deleted")
}

func SessionKey(sessionKey string) UserHandlerConfig {
	return func(uh *UserHandler) error {
		uh.sessionKey = sessionKey
		return nil
	}
}

func AuthSuccessRoute(authSuccessRoute string) UserHandlerConfig {
	return func(uh *UserHandler) error {
		uh.authSuccessRoute = authSuccessRoute
		return nil
	}
}

func LoginRoute(loginRoute string) UserHandlerConfig {
	return func(uh *UserHandler) error {
		uh.loginRoute = loginRoute
		return nil
	}
}

func SetupRoute(setupRoute string) UserHandlerConfig {
	return func(uh *UserHandler) error {
		uh.setupRoute = setupRoute
		return nil
	}
}

func LoginTplKey(loginTplKey string) UserHandlerConfig {
	return func(uh *UserHandler) error {
		uh.loginTplKey = loginTplKey
		return nil
	}
}

func SetupTplKey(setupTplKey string) UserHandlerConfig {
	return func(uh *UserHandler) error {
		uh.setupTplKey = setupTplKey
		return nil
	}
}

func SessionTokenKey(sessionTokenKey string) UserHandlerConfig {
	return func(uh *UserHandler) error {
		uh.sessionTokenKey = sessionTokenKey
		return nil
	}
}

func RememberMeCookieKey(rememberMeCookieKey string) UserHandlerConfig {
	return func(uh *UserHandler) error {
		uh.rememberMeCookieKey = rememberMeCookieKey
		return nil
	}
}
