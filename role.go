package gousers

import (
	"errors"
	"strings"
)

type UserRole string

const (
	RoleAdmin      UserRole = "Admin"
	RoleSupervisor          = "Supervisor"
	RoleEditor              = "Editor"
)

func (ur UserRole) IsValid() error {
	switch ur {
	case RoleAdmin, RoleSupervisor, RoleEditor:
		return nil
	}
	return errors.New("invalid user role")
}

func (ur *UserRole) UnmarshalJSON(b []byte) error {
	roleType := UserRole(strings.Trim(string(b), `"`))
	switch roleType {
	case RoleAdmin, RoleSupervisor, RoleEditor:
		*ur = roleType
		return nil
	}
	return errors.New("invalid uer role in json unmarshal")
}
