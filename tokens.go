package gousers

import (
	"crypto/rand"
	"encoding/base64"
)

func GenerateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}
	return b, nil
}

func GenerateRandomURLSafeString(n int) (string, error) {
	bs, err := GenerateRandomBytes(n)
	return base64.URLEncoding.EncodeToString(bs), err
}

