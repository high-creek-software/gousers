module gitlab.com/high-creek-software/gousers

go 1.15

require (
	github.com/google/uuid v1.1.2
	github.com/gorilla/sessions v1.2.1 // indirect
	gitlab.com/kendellfab/fazer v0.11.2
	gitlab.com/kendellfab/web v0.4.0
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	gorm.io/gorm v1.20.5
)
