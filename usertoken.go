package gousers

type UserToken struct {
	ID              string `json:"id"`
	UserID          string `json:"userId"`
	Token           string `json:"token"`
	Fingerprint     string `json:"fingerprint"`
	RememberMeToken string `json:"rememberMeToken"`
}

type ApiUserToken struct {
	ID          string `json:"id"`
	Fingerprint string `json:"fingerprint"`
}
