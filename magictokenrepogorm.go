package gousers

import (
	"time"

	"github.com/google/uuid"

	"gorm.io/gorm"
)

type MagicTokenRepoGorm struct {
	db *gorm.DB
}

func (m MagicTokenRepoGorm) Store(userID, token string) (string, error) {
	gmt := &gormMagicToken{ID: uuid.New().String(), UserID: userID, Token: token}
	err := m.db.Create(gmt).Error
	return gmt.ID, err
}

func (m MagicTokenRepoGorm) Find(id string) (MagicToken, error) {
	var gmt gormMagicToken
	err := m.db.Where(&gormMagicToken{ID: id}).First(&gmt).Error
	if err != nil {
		return MagicToken{}, err
	}

	return MagicToken{ID: gmt.ID, UserID: gmt.UserID, Token: gmt.Token, CreatedAt: gmt.CreatedAt}, nil
}

func (m MagicTokenRepoGorm) Delete(id string) error {
	return m.db.Delete(&gormMagicToken{ID: id}).Error
}

func NewMagicTokenRepoGorm(db *gorm.DB) MagicTokenRepo {
	magicRepo := &MagicTokenRepoGorm{db: db}
	db.AutoMigrate(&gormMagicToken{})
	return magicRepo
}

type gormMagicToken struct {
	ID        string `gorm:"type:varchar(48)"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
	UserID    string     `gorm:"type:varchar(48)"`
	Token     string     `gorm:"type:varchar(512);index:magic_token_token_idx"`
}

func (gormMagicToken) TableName() string {
	return "magic_token"
}
