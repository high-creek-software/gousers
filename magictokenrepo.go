package gousers

type MagicTokenRepo interface {
	Store(userID, token string) (string, error)
	Find(id string) (MagicToken, error)
	Delete(id string) error
}
