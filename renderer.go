package gousers

import (
	"github.com/gorilla/sessions"
	"io"
	"net/http"
)

type Renderer interface {
	SetErrorFlash(w http.ResponseWriter, r *http.Request, message string)
	SetSuccessFlash(w http.ResponseWriter, r *http.Request, message string)
	Redirect(w http.ResponseWriter, r *http.Request, url string)
	RenderPage(w http.ResponseWriter, r *http.Request, key string, data map[string]interface{})
	RenderJson(w http.ResponseWriter, data interface{})
	Get(r *http.Request, key string) (*sessions.Session, error)
}

type Messenger interface {
	RenderMessagePlain(w io.Writer, key string, data map[string]interface{}) error
	RenderMessageHtml(w io.Writer, key string, data map[string]interface{}) error
}