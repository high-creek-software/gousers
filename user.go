package gousers

import (
	"context"
	"net/http"
	"time"
)

type User struct {
	ID        string    `json:"id"`
	Email     string    `json:"email"`
	Name      string    `json:"name"`
	Avatar    string    `json:"avatar"`
	Bio       string    `json:"bio"`
	UserRole  int       `json:"userRole"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
}

type CreateUser struct {
	User
	Password string `json:"password"`
}

type key int

var contextKey = key(22)
var tokenKey = key(24)

func SetUser(usr User, r *http.Request) {
	ctx := r.Context()
	ctx = context.WithValue(ctx, contextKey, usr)
	*r = *(r.WithContext(ctx))
}

func FromContext(r *http.Request) (User, bool) {
	usr, ok := r.Context().Value(contextKey).(User)
	return usr, ok
}

func SetToken(token string, r *http.Request) {
	ctx := r.Context()
	ctx = context.WithValue(ctx, tokenKey, token)
	*r = *(r.WithContext(ctx))
}

func TokenFromContext(r *http.Request) (string, bool) {
	token, ok := r.Context().Value(tokenKey).(string)
	return token, ok
}
